package utils;

import static java.lang.Double.parseDouble;
import static java.lang.String.format;
//import static java.awt.geom.Point2D.distance;
import static java.lang.String.valueOf;
import static javax.xml.transform.OutputKeys.INDENT;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import model.Point;

public class Parsers { // TODO XML do osobnej klasy

	public static String[] parseTemporaryOutput(String line) {
		return line.split("\\|");
	}

	public static String[] parseErrorOutput(String line) {
		return line.split("\\|");
	}

	public static void generateTestFiles(int numOfCities, int numOfFiles) {

		for (int i = 0; i < numOfFiles; i++) {
			List<Point> lista = generateRandomCitiesList(numOfCities);
			
			createXmlWithCities(lista, "test" + format("%03d", i) + ".xml");
		}

	}

	public static void createXmlWithCities(List<Point> cities, String name) {

		try {

			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument(); // pociąg

			Element rootElement = doc.createElement("cities");
			doc.appendChild(rootElement);

			for (int i = 0; i < cities.size(); i++) {

				Element city = doc.createElement("city");
				city.setAttribute("id", valueOf(i));
				city.setAttribute("x", valueOf(cities.get(i).getX()));
				city.setAttribute("y", valueOf(cities.get(i).getY()));
				rootElement.appendChild(city);

				for (int j = 0; j < cities.size(); j++) {

					Element distance = doc.createElement("distance");
					distance.setAttribute("toCity", valueOf(j));
					distance.appendChild(doc.createTextNode(
							i == j ? "-1" : valueOf(countDistanceBetweenTwoCities(cities.get(i), cities.get(j)))));

					city.appendChild(distance);
				}
			}

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(name));

			transformer.setOutputProperty(INDENT, "yes"); // zeby robił entery
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2"); // i
																								// taby

			transformer.transform(source, result);

			System.out.println("Jupikajej!");

		} catch (ParserConfigurationException | TransformerException e) {

			e.printStackTrace();
		}
	}

	public static List<Point> readXmlIntoNodesList(String fileName) {

		List<Point> cities = new ArrayList<>();

		try {

			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("city");

			for (int i = 0; i < nodeList.getLength(); i++) {

				Node node = nodeList.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) node;

					String xParam = element.getAttribute("x");
					String yParam = element.getAttribute("y");

					if (xParam.isEmpty() || yParam.isEmpty()) {
						return new ArrayList<>();
					}

					cities.add(
							new Point(parseDouble(element.getAttribute("x")), parseDouble(element.getAttribute("y"))));
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {

			e.printStackTrace();
		}

		return cities;
	}

	private static String[] convertNumberBooleansToText(String[] array) {

		array[0] = array[0].replace("1", "tak|").replace("0", "nie|").substring(0, 11);
		array[array.length - 1] = array[array.length - 1].replace("1", "tak").replace("0", "nie");

		return array;
	}

	private static double countDistanceBetweenTwoCities(Point first, Point second) {

		return Math.hypot(first.getX() - second.getX(), first.getY() - second.getY());
	}

	public static String[] parseTemporaryOutputLine(String s) {
		String[] result = s.split("\\s+");
		return result;
	}

	private static List<Point> generateRandomCitiesList(int numOfCities) {

		List<Point> list = new ArrayList<>();
		Random random = new Random();

		for (int i = 0; i < numOfCities; i++) {
			list.add(new Point(random.nextDouble() * 500.0, random.nextDouble() * 500.0));
		}

		return list;
	}
}
