package utils;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.TextField;

public class Validators {

	private static final String RED_STYLE = "-fx-base: #FF0000";

	/**
	 * Walidator sprawdzajacy, czy podany string zawiera tylko liczbe calkowita dowolnej dlugosci
	 * @param s - <code>String</code> zawierajacy tekst do analizowania
	 * @return <code>true</code> jesli <code>s</code> zawiera tylko liczbe calkowita dowolnej dlugosci<br><code>false</code> w przeciwnym wypadku
	 */
	public static boolean isNumber(String s) {
		return s.matches("\\d+") ? true : false;
	}
	
	public static boolean hasXmlExtension(String s) {
		return s.contains(".xml") ? true : false;
	}
	
	public static boolean isInPercentRange(String s) {
		if(isNumber(s)) {
			int value = Integer.parseInt(s);
			return value >= 0 && value <= 100 ? true : false;
		} else 
			return false;
	}
	
	public static ChangeListener<? super Boolean> xmlExtensionListener(TextField field) {
		return (evt, oldValue, newValue) -> {
			if (!newValue) {
				if (!Validators.hasXmlExtension(field.getText()))
					field.setStyle(RED_STYLE);
				else
					field.setStyle("");
			}
		};
	}
	

	public static ChangeListener<? super Boolean> percentRangeListener(TextField field) {
		return (evt, oldValue, newValue) -> {
			if (!newValue) {
				if (!Validators.isInPercentRange(field.getText()))
					field.setStyle(RED_STYLE);
				else
					field.setStyle("");
			}
		};
	}
	
	public static ChangeListener<? super Boolean> isNumberListener(TextField field) {
		return (evt, oldValue, newValue) -> {
			if (!newValue) {
				if (!isNumber(field.getText()))
					field.setStyle(RED_STYLE);
				else
					field.setStyle("");
			}
		};
	}
	
	public static boolean isNullOrEmpty(String string){
		return string == null || "".equals(string);
	}
}
