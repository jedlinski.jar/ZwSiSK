package controller;

import static java.lang.String.format;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import app.Main;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import utils.Parsers;

public class ChartsController implements Initializable {

	MainViewController mainViewController; // TODO przekazywać wyniki jako
											// tablicę czy coś

	@FXML
	private GridPane chartsGrid;

	private List<List<Data<Number, Number>>> lineChartData = new ArrayList<List<Data<Number, Number>>>();
	private List<List<Data<String, Number>>> barChartData = new ArrayList<List<Data<String, Number>>>();

	@FXML
	private TextField specimensField2, islandsField2, iterationsField2, changesField2, mutationField2, migrationField2,
			specimensField3, islandsField3, iterationsField3, changesField3, mutationField3, migrationField3, repsField,
			citiesField;

	@FXML
	private CheckBox bfCheck, bfGPUCheck, genCheck, genThreadsCheck, greedyCheck2, greedyCheck3;

	private LineChart<Number, Number> lineChart;
	private NumberAxis citiesAxis, timeAxis;

	private BarChart<String, Number> barChart;
	private CategoryAxis algorithmAxis;
	private NumberAxis deviationAxis;

	private double bruteforceResult = -1;

	@Override
	public void initialize(URL url, ResourceBundle res) {
		mainViewController = Main.getMainViewController();

		for (int i = 0; i < 4; i++) {
			lineChartData.add(new ArrayList<Data<Number, Number>>());
		}

		for (int i = 0; i < 6; i++) {
			barChartData.add(new ArrayList<Data<String, Number>>());
		}

		citiesAxis = new NumberAxis();
		timeAxis = new NumberAxis();
		citiesAxis.setLabel("Liczba miast");
		timeAxis.setLabel("Czas [ms]");
		lineChart = new LineChart<Number, Number>(citiesAxis, timeAxis);
		// lineChart.setTitle(""); // TODO tytuł wykresu

		algorithmAxis = new CategoryAxis();
		deviationAxis = new NumberAxis();
		algorithmAxis.setLabel("Algorytm");
		deviationAxis.setLabel("Odchylenie od alg. bruteforce");
		barChart = new BarChart<String, Number>(algorithmAxis, deviationAxis);

		chartsGrid.add(lineChart, 0, 0);
		chartsGrid.add(barChart, 0, 1);

	}

	/**
	 * 
	 * @param title
	 *            tytuł serii
	 * @param algorithm
	 *            numer algorytmu: 0 - bruteforce, 1 - bruteforce GPU, 2 -
	 *            genetyczny, 3 - genetyczny na wątkach
	 * @return seria
	 */
	public Series<Number, Number> addSeries(String title, int algorithm) {
		XYChart.Series<Number, Number> series = new Series<Number, Number>();
		series.setName(title);

		for (Data<Number, Number> d : lineChartData.get(algorithm)) {
			series.getData().add(d);
		}

		return series;
	}

	private String prepareTestCommand(int algorithm, String filename) { // TODO
																		// do
																		// utilsow,
																		// rzucac
																		// fancy
																		// input
																		// exception
		StringBuilder builder;
		switch (algorithm) {
		case 0:
			builder = new StringBuilder("ZwSiSK ");
			builder.append("100").append(" ");
			builder.append(filename);
			break;
		case 1:
			builder = new StringBuilder("GPU ");
			builder.append(filename);
			break;
		case 2:
			builder = new StringBuilder("ZwSiSK ");
			builder.append("010").append(" ");
			builder.append(filename).append(" ");
			builder.append(specimensField2.getText()).append(" ");
			builder.append(islandsField2.getText()).append(" ");
			builder.append(iterationsField2.getText()).append(" ");
			builder.append(changesField2.getText()).append(" ");
			builder.append(mutationField2.getText()).append(" ");
			builder.append(migrationField2.getText()).append(" ");
			builder.append(greedyCheck2.isSelected() ? 1 : 0);
			break;
		case 3:
			builder = new StringBuilder("ZwSiSK ");
			builder.append("001").append(" ");
			builder.append(filename).append(" ");
			builder.append(specimensField3.getText()).append(" ");
			builder.append(islandsField3.getText()).append(" ");
			builder.append(iterationsField3.getText()).append(" ");
			builder.append(changesField3.getText()).append(" ");
			builder.append(mutationField3.getText()).append(" ");
			builder.append(migrationField3.getText()).append(" ");
			builder.append(greedyCheck3.isSelected() ? 1 : 0);
			break;
		default:
			builder = null;
			break;
		}

		return builder.toString();
	}

	@FXML
	public void runTests() {
		int numberOfCities = Integer.parseInt(citiesField.getText()),
				numberOfReps = Integer.parseInt(repsField.getText());
		Parsers.generateTestFiles(numberOfCities, numberOfReps);
		if (bfCheck.isSelected()) {
			double d = testBruteforce(numberOfReps);
			lineChartData.get(0).add(new Data<Number, Number>(numberOfCities, d));
		}
		if (bfGPUCheck.isSelected()) {
			double d = testBruteforceGPU(numberOfReps);
			lineChartData.get(1).add(new Data<Number, Number>(numberOfCities, d));
		}
		if (genCheck.isSelected()) {
			double d[] = testGenetic(numberOfReps);
			lineChartData.get(2).add(new Data<Number, Number>(numberOfCities, d[0]));
			barChartData.get(0).add(new Data<String, Number>("", Math.abs(1 - d[1] / bruteforceResult) * 100));
			barChartData.get(1).add(new Data<String, Number>("", Math.abs(1 - d[2] / bruteforceResult) * 100));
			barChartData.get(2).add(new Data<String, Number>("", Math.abs(1 - d[3] / bruteforceResult) * 100));
		}

		if (genThreadsCheck.isSelected()) {
			double d[] = testGeneticOnThreads(numberOfReps);
			lineChartData.get(3).add(new Data<Number, Number>(numberOfCities, d[0]));
			barChartData.get(3).add(new Data<String, Number>("", Math.abs(1 - d[1] / bruteforceResult) * 100));
			barChartData.get(4).add(new Data<String, Number>("", Math.abs(1 - d[2] / bruteforceResult) * 100));
			barChartData.get(5).add(new Data<String, Number>("", Math.abs(1 - d[3] / bruteforceResult) * 100));
		}
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Testy");
		alert.setHeaderText(null);
		alert.setContentText("Testy zako�czone!");

		alert.showAndWait();
	}

	@FXML
	public void printCharts() {
		lineChart.getData().clear();
		barChart.getData().clear();

		for (List<Data<Number, Number>> l : lineChartData) {
			XYChart.Series<Number, Number> series = new Series<Number, Number>();
			switch (lineChartData.indexOf(l)) {
			case 0:
				series.setName("Bruteforce");
				break;
			case 1:
				series.setName("Bruteforce GPU");
				break;
			case 2:
				series.setName("Genetyczny");
				break;
			case 3:
				series.setName("Genetyczny na w�tkach");
				break;
			default:
				break;
			}

			for (Data<Number, Number> d : l) {
				series.getData().add(d);
			}

			lineChart.getData().add(series);
		}

		if (bruteforceResult != -1) {
			for (List<Data<String, Number>> l : barChartData) {
				XYChart.Series<String, Number> series = new Series<String, Number>();
				switch (barChartData.indexOf(l)) {
				case 0:
					series.setName("Genetyczny - najgorsze");
					break;
				case 1:
					series.setName("Genetyczny - �rednie");
					break;
				case 2:
					series.setName("Genetyczny - najlepsze");
					break;
				case 3:
					series.setName("Genetyczny na watkach - najgorsze");
					break;
				case 4:
					series.setName("Genetyczny na watkach - �rednie");
					break;
				case 5:
					series.setName("Genetyczny na watkach - najlepsze");
					break;
				default:
					break;
				}

				for (Data<String, Number> d : l) {
					series.getData().add(d);
				}
				barChart.getData().add(series);
			}
		}
	}

	public double testBruteforce(int numberOfTests) {
		double[] result = new double[numberOfTests];
		bruteforceResult = -1;
		for (int i = 0; i < numberOfTests; i++) {
			try {
				ExecutorService executor = Executors.newFixedThreadPool(1);

				final String filename = "test" + format("%03d", i) + ".xml";

				Future<Double> future = executor.submit(() -> {
					Runtime r = Runtime.getRuntime();
					Process p = r.exec(prepareTestCommand(0, filename));
					double d = 0;
					BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						if (inputLine.contains("BT")) {
							String[] toSplit = inputLine.split("\\|");
							d = Double.parseDouble(toSplit[3]);
							if (bruteforceResult == -1)
								bruteforceResult = Double.parseDouble(toSplit[2]);
						} else {
							if (inputLine.length() > 1) {
								System.out.println(inputLine + "\n");
							}
						}
					}
					in.close();
					return d;
				});

				result[i] = future.get();
				executor.shutdown();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		double sum = 0.0;
		for (double d : result) {
			sum += d;
		}
		return sum / numberOfTests;
	}

	public double testBruteforceGPU(int numberOfTests) {
		double[] result = new double[numberOfTests];
		for (int i = 0; i < numberOfTests; i++) {
			try {
				ExecutorService executor = Executors.newFixedThreadPool(1);
				final String filename = "test" + format("%03d", i) + ".xml";

				Future<Double> future = executor.submit(() -> {
					Runtime r = Runtime.getRuntime();
					Process p = r.exec(prepareTestCommand(1, filename));
					double d = 0;
					BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						if (inputLine.contains("GPU")) {
							String[] toSplit = inputLine.split("\\|");
							d = Double.parseDouble(toSplit[3]);
						} else {
							if (inputLine.length() > 1) {
								System.out.println(inputLine + "\n");
							}
						}
					}
					in.close();
					return d;
				});

				result[i] = future.get();
				executor.shutdown();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		double sum = 0.0;
		for (double d : result) {
			sum += d;
		}
		return sum / numberOfTests;
	}

	public double[] testGenetic(int numberOfTests) {
		double[][] result = new double[numberOfTests][2];
		for (int i = 0; i < numberOfTests; i++) {
			try {
				ExecutorService executor = Executors.newFixedThreadPool(1);
				final String filename = "test" + format("%03d", i) + ".xml";

				Future<double[]> future = executor.submit(() -> {
					Runtime r = Runtime.getRuntime();
					Process p = r.exec(prepareTestCommand(2, filename));
					double[] d = new double[2];
					BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						if (inputLine.contains("GEN")) {
							String[] toSplit = inputLine.split("\\|");
							d[0] = Double.parseDouble(toSplit[3]);
							d[1] = Double.parseDouble(toSplit[2]);

						} else {
							if (inputLine.length() > 1) {
								System.out.println(inputLine + "\n");
							}
						}
					}
					in.close();
					return d;
				});

				result[i] = future.get();
				executor.shutdown();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		double sum = 0.0;
		double resultSum = 0.0;
		double bestResult = Double.MAX_VALUE, worstResult = Double.MIN_VALUE;
		for (double[] d : result) {
			sum += d[0];
			resultSum += d[1];
			if (d[1] < bestResult)
				bestResult = d[1];
			if (d[1] > worstResult)
				worstResult = d[1];
		}
		return new double[] { (sum / numberOfTests), worstResult, (resultSum / numberOfTests), bestResult };
	}

	public double[] testGeneticOnThreads(int numberOfTests) {
		double[][] result = new double[numberOfTests][2];
		for (int i = 0; i < numberOfTests; i++) {
			try {
				ExecutorService executor = Executors.newFixedThreadPool(1);
				final String filename = "test" + format("%03d", i) + ".xml";

				Future<double[]> future = executor.submit(() -> {
					Runtime r = Runtime.getRuntime();
					Process p = r.exec(prepareTestCommand(3, filename));
					double[] d = new double[2];
					BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						if (inputLine.contains("THR")) {
							String[] toSplit = inputLine.split("\\|");
							d[0] = Double.parseDouble(toSplit[3]);
							d[1] = Double.parseDouble(toSplit[2]);
						} else {
							if (inputLine.length() > 1) {
								System.out.println(inputLine + "\n");
							}
						}
					}
					in.close();
					return d;
				});

				result[i] = future.get();
				executor.shutdown();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		double sum = 0.0;
		double resultSum = 0.0;
		double bestResult = Double.MAX_VALUE, worstResult = Double.MIN_VALUE;
		for (double[] d : result) {
			sum += d[0];
			resultSum += d[1];
			if (d[1] < bestResult)
				bestResult = d[1];
			if (d[1] > worstResult)
				worstResult = d[1];
		}
		return new double[] { (sum / numberOfTests), worstResult, (resultSum / numberOfTests), bestResult };
	}
}
