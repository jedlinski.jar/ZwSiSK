package controller;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static javafx.scene.input.MouseEvent.MOUSE_CLICKED;
import static utils.Parsers.createXmlWithCities;
import static utils.Parsers.parseErrorOutput;
import static utils.Parsers.parseTemporaryOutput;
import static utils.Validators.isNullOrEmpty;
import static utils.Validators.isNumberListener;
import static utils.Validators.percentRangeListener;
import static utils.Validators.xmlExtensionListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import app.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.Point;
import utils.Parsers;

public class MainViewController implements Initializable {

	private static final String RED_STYLE = "-fx-base: #FF0000";

	// TODO ladnie pogrupowac zmienne
	@FXML
	private TextArea console;
	@FXML
	private TextField fileField, specimensField, islandsField, iterationsField, changesField, mutationField,
			migrationField, resultField, timeField, xmlField;
	@FXML
	private CheckBox bfCheck, bfGraphicCardCheck, genCheck, genThreadsCheck, greedyCheck, bfDraw, bfGraphicDraw,
			genDraw, genThreadsDraw;
	@FXML
	private Canvas canvas;

	private static final double CANVAS_SIZE = 500, NODE_RADIUS = 8;

	private GraphicsContext graphicsContext;

	private List<Point> nodeList = new ArrayList<>();

	private String bfResult, bfGraphicCardResult, genResult, genThreadsResult;

	final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	private List<TextField> parametersFields = new ArrayList<>();

	@FXML
	private AnchorPane chartsAnchor;

	@Override
	public void initialize(URL url, ResourceBundle res) {
		Main.setController(this);
		graphicsContext = canvas.getGraphicsContext2D();
		canvas.addEventHandler(MOUSE_CLICKED, this::mouseOnCanvasClicked);

		fileField.focusedProperty().addListener(xmlExtensionListener(fileField));

		migrationField.focusedProperty().addListener(percentRangeListener(migrationField));

		mutationField.focusedProperty().addListener(percentRangeListener(mutationField));

		xmlField.focusedProperty().addListener(xmlExtensionListener(xmlField));
		
		specimensField.focusedProperty().addListener(isNumberListener(specimensField));
		
		islandsField.focusedProperty().addListener(isNumberListener(islandsField));
		
		iterationsField.focusedProperty().addListener(isNumberListener(iterationsField));
		
		changesField.focusedProperty().addListener(isNumberListener(changesField));

		drawAxes();

		try {
			GridPane chartsGrid = FXMLLoader.load(getClass().getResource("/view/ChartsView.fxml"));
			chartsAnchor.getChildren().add(chartsGrid);
		} catch (IOException e1) {
			System.err.println("Nie udało się dodać panelu z wykresami!");
			e1.printStackTrace();
		}

		parametersFields.addAll(asList(fileField, specimensField, islandsField, iterationsField, changesField,
				mutationField, migrationField));
	}

	private void mouseOnCanvasClicked(MouseEvent e) {
		System.out.println(e.getX() + " " + e.getY());
		Point n = new Point(e.getX(), e.getY());
		nodeList.add(n);
		drawPoint(n);
	}

	private void executeCommand(String cmd) {
		scheduler.schedule(new Runnable() {
			@Override
			public void run() {
				System.out.println(cmd);
				try {
					Runtime r = Runtime.getRuntime();
					Process p = r.exec(cmd);

					BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						if (inputLine.contains("BT")) {
							String[] result = inputLine.split("\\|");
							bfResult = result[1];
							console.appendText("----------------- BRUTEFORCE -----------------\n");
							console.appendText("Wynik: " + result[2] + "\nCzas: " + result[3] + "\n");
						} else if (inputLine.contains("GEN")) {
							String[] result = inputLine.split("\\|");
							genResult = result[1];
							console.appendText("------------- ALGORYTM GENETYCZNY -------------\n");
							console.appendText("Wynik: " + result[2] + "\nCzas: " + result[3] + "\n");
						} else if (inputLine.contains("THR")) {
							String[] result = inputLine.split("\\|");
							genThreadsResult = result[1];
							console.appendText("---------- ALG. GENETYCZNY NA WATKACH ----------\n");
							console.appendText("Wynik: " + result[2] + "\nCzas: " + result[3] + "\n");
						} else if (inputLine.contains("OUT")) {
							System.out.println("OUT: " + inputLine);
							String[] result = parseTemporaryOutput(inputLine);
							console.appendText(result[1] + "\n");
							resultField.setText(result[2]);
							timeField.setText(result[3] + " ms");
						} else if (inputLine.contains("ERR")) {
							String[] result = parseErrorOutput(inputLine);
							console.appendText(result[1] + "\n");
							break;
						} else if (inputLine.contains("GPU")) {
							String[] result = inputLine.split("\\|");
							bfGraphicCardResult = result[1];
							console.appendText("---------- BRUTEFORCE NA KARCIE GRAFICZNEJ ----------\n");
							console.appendText("Wynik: " + result[2] + "\nCzas: " + result(result[3]) + "\n");
						} else {
							if (inputLine.length() > 1) { // TEMPFIX
								console.appendText(inputLine + "\n");
								// markPath(Parsers.parseTemporaryOutputLine(inputLine));
							}
						}
					}
					in.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
				
				drawAllChosenPaths();
			}
		}, 0, SECONDS);

	}

	private void clearResuls() {
		bfResult = "";
		bfGraphicCardResult = "";
		genResult = "";
		genThreadsResult = "";
	}

	@FXML
	private void runTheProgram() {

		clearResuls();

		if (genCheck.isSelected() || genThreadsCheck.isSelected()) {
			parametersFields.stream().forEach(this::markFieldIfEmpty);

			if (parametersFields.stream().anyMatch(field -> isNullOrEmpty(field.getText()))) {
				return;
			}
		}

		if (bfCheck.isSelected() || genCheck.isSelected() || genThreadsCheck.isSelected()) {
			executeCommand(prepareCommand());
		}

		if (bfGraphicCardCheck.isSelected()) {
			executeCommand("GPU " + fileField.getText());
		}

	}

	private void markFieldIfEmpty(TextField field) {
		if (isNullOrEmpty(field.getText())) {
			field.setStyle(RED_STYLE);
		}
	}

	private String prepareCommand() { // TODO do utilsow, rzucac fancy input
										// exception
		StringBuilder builder = new StringBuilder("ZwSiSK ");
		if (specimensField.getText().isEmpty() || islandsField.getText().isEmpty()
				|| iterationsField.getText().isEmpty() || changesField.getText().isEmpty()
				|| mutationField.getText().isEmpty() || migrationField.getText().isEmpty()) { // TODO
																								// obowiazkowo
																								// lepsza
																								// walidacja
																								// -
																								// krotki
																								// czy
																								// dlugi
																								// input
			builder.append(getMask()).append(" ");
			builder.append(fileField.getText());
		} else {
			builder.append(getMask()).append(" ");
			builder.append(fileField.getText()).append(" ");
			builder.append(specimensField.getText()).append(" ");
			builder.append(islandsField.getText()).append(" ");
			builder.append(iterationsField.getText()).append(" ");
			builder.append(changesField.getText()).append(" ");
			builder.append(mutationField.getText()).append(" ");
			builder.append(migrationField.getText()).append(" ");
			builder.append(greedyCheck.isSelected() ? 1 : 0);
		}
		return builder.toString();
	}

	private String getMask() {
		StringBuilder builder = new StringBuilder();
		builder.append(bfCheck.isSelected() ? 1 : 0);
		builder.append(genCheck.isSelected() ? 1 : 0);
		builder.append(genThreadsCheck.isSelected() ? 1 : 0);
		return builder.toString();
	}

	@FXML
	private void drawAllChosenPaths() {

		clearCanvas();
		drawAxes();
		drawAllPoints();

		if (bfDraw.isSelected() && !isNullOrEmpty(bfResult)) {
			markPath(bfResult, Color.GREEN);
		}

		if (bfGraphicDraw.isSelected() && !isNullOrEmpty(bfGraphicCardResult)) {
			markPath(bfGraphicCardResult, Color.CHARTREUSE);
		}

		if (genDraw.isSelected() && !isNullOrEmpty(genResult)) {
			markPath(genResult, Color.BLUE);
		}

		if (genThreadsDraw.isSelected() && !isNullOrEmpty(genThreadsResult)) {
			markPath(genThreadsResult, Color.RED);
		}

		graphicsContext.setStroke(Color.BLACK);
	}

	// ---------- RYSOWANIE / PRAWY PANEL ----------

	private void clearCanvas() {
		graphicsContext.clearRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);
	}

	private void drawAxes() {
		double w = canvas.getWidth(), h = canvas.getHeight(), midW = w / 2.0, midH = h / 2.0;
		graphicsContext.strokeLine(midW, 0.0, midW, h);
		graphicsContext.strokeLine(0.0, midH, w, midH);
		graphicsContext.fillPolygon(new double[] { midW, midW - 4.0, midW + 4.0 }, new double[] { 0.0, 8.0, 8.0 }, 3);
		graphicsContext.fillPolygon(new double[] { w, w - 8.0, w - 8.0 }, new double[] { midH, midH - 4.0, midH + 4.0 },
				3);
	}

	private void drawNode(double x, double y) {
		graphicsContext.fillOval(x - NODE_RADIUS, y - NODE_RADIUS, NODE_RADIUS * 2, NODE_RADIUS * 2);
	}

	private void connectNodes(int firstIndex, int secondIndex) {
		Point first = nodeList.get(firstIndex), second = nodeList.get(secondIndex);
		graphicsContext.strokeLine(first.getX(), first.getY(), second.getX(), second.getY());
	}

	private void markPath(String s, Color c) {
		String[] str = s.split("\\s+");
		graphicsContext.setStroke(c);
		for (int i = 0; i < nodeList.size() - 1; i++) {
			connectNodes(parseInt(str[i]), parseInt(str[i + 1]));
		}
		connectNodes(parseInt(str[nodeList.size() - 1]), parseInt(str[0]));
	}

	@FXML
	private void clearAction() {
		nodeList.clear();
		clearCanvas();
		drawAxes();
	}

	@FXML
	private void infoAction() {
		console.appendText("#\tX\tY\n");
		for (int i = 0; i < nodeList.size(); i++)
			console.appendText(i + "\t" + nodeList.get(i).toString() + "\n");
	}

	@FXML
	private void generateXMLAction() {
		if (!nodeList.isEmpty()) {
			createXmlWithCities(nodeList, xmlField.getText());
			fileField.setText(xmlField.getText());
		}
	}

	@FXML
	private void readXMLAction() {

		clearAction();
		nodeList = Parsers.readXmlIntoNodesList(xmlField.getText());

		if (nodeList.isEmpty()) {
			console.appendText("Nie wszystkie node'y miały podane współrzędne!\n");
		}

		drawAllPoints();
	}

	private void drawAllPoints() {
		nodeList.stream().forEach(this::drawPoint);
	}

	private void drawPoint(Point node) {
		drawNode(node.getX(), node.getY());
	}
	
	private String result(String result){
		double wynik = Double.parseDouble(result);
		
		if(nodeList.size() < 12){
			return ((long) wynik/2) + ((wynik - (long) wynik)/2.0) + "";
		}
		return result;
	}
}
