package app;
	
import controller.ChartsController;
import controller.MainViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;


public class Main extends Application {
	
	private static MainViewController mainViewController;
	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			TabPane root = FXMLLoader.load(getClass().getResource("/view/MainView.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/resources/style.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("ZWSISK - Jedlinski, Romanowicz, Soltysiak");
			primaryStage.show();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public static void setController(MainViewController mvc) {
		mainViewController = mvc;
	}
	
	public static MainViewController getMainViewController() {
		return mainViewController;
	}
	
	public ChartsController getChartsController() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ChartsView.fxml"));
		return (ChartsController) loader.getController();
	}
}
